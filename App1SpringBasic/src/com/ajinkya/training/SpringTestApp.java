package com.ajinkya.training;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringTestApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext context = new ClassPathXmlApplicationContext("file:config/spring.xml");
		HelloWorld helloWorld= (HelloWorld) context.getBean("helloWorld");
		helloWorld.getMessage();
		
	}

}
